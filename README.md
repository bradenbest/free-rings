# Free Rings

A FOSS recreation of Ninjakiwi's 2007 flash game, Rings, in HTML5.

Livestream series where I developed it:
https://www.youtube.com/playlist?list=PLALx9d_ycE5uirkxkRsLFUqIMdwJptzIB

Play it [here](https://itch.io/bradenbest/free-rings)

![Screenshot](rings-prev.png)
