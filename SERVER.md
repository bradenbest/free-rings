## API Base URL

The source code uses "localhost" as the baseurl.

The URL used by the release build is
https://freerings.azurewebsites.net/api

## Where's the server code?

The server that the release build talks to is not open source.

However, I will document the API here so that you can implement your own.

## The API

LeMorrow and I worked together to define a simple JSON REST API for
submitting runs, fetching leaderboard data, changing your leaderboard name,
and reporting suspicious scores.

## Data Types

I will use this format to describe data types:
```
TypeName: type
    description of type

TypeName: struct
    description of type

    field: type
        description of field
    ...
```

`TypeName` is just a name. I will refer back to it later in the documentation. To the right is what it is an alias of.
The fields under it are typed fields.

Represented as a JSON object, an instance of `GenericError` might look like this `{"message": "Could not find run"}`

I will use similar semantics to describe the requests and responses.

And now, the data types:
```
GenericError: struct
    returned from an HTTP error. The JSON body should contain the details.

    message: string
        string describing the error. Go into detail.

UnixMSTimestamp: long
    an integer type represending a number of milliseconds passed since
    the unix epoch. JavaScript's `Date.now()` returns this.

LBEntry: struct
    a leaderboard entry from the server

    id: int
        the ID of the run. Can be used by the client to report the run.

    position: int
        the run's position in the leaderboard. The WR would be position 1.

    name: string
        the name attached to the run

    score: int
        the score of the run

    round: int
        the round reached

    clears: int
        the number of clears

    startTimestamp: UnixMSTimestamp
        the timestamp when the run started. A UnixMSTimestamp is the type
        of timestamp JavaScript's `Date.now()` function returns. It must
        have millisecond granularity.

    endTimestamp: UnixMSTimestamp
        the timestamp when the run ended

RunSlice: struct
    a slice represents a snapshot of the run at a specific timestamp.
    These are provided with a run and should be used for validation.

    score: int
        the score

    round
        the round reached

    clears
        the number of clears

    timeStamp
        the timestamp of this slice

SubmitRunData: struct
    a run submitted by the client

    userToken: string
        the user's uuid

    score: int
        the score of the run

    round: int
        the round reached

    clears: int
        the number of clears

    startTime: UnixMSTimestamp
        start time of the run

    endTime: UnixMSTimestamp
        end time of the run

    slices: array<RunSlice>
```

## Errors

On error, return the appropriate HTTP code with a `GenericError` body. The client expects a `2XX` code for successful
responses. Anything else will be assumed to be an error and will be handled according to their error code. If the error
code is not listed, then the default handler will fire, which is to echo the embedded error message in the console.

## Leaderboard fetching
```
Request: GET /runs
    Offset: int
        the offset of the leaderboard to fetch (0 = position 1)

    Limit: int
        the number of entries to return

Response: 200 OK
     total: int
         the total number of scores in the database

     runs: array<LBEntry>
         list of LBEntry
```
This endpoint accepts an offset and a limit and returns a slice of the leaderboard sorted by score.
For example, if Offset is 0 and Limit is 10, then the top 10 scores should be returned. If Offset is 20 and Limit is 5,
then leaderboard positions 21-25 are returned.

If Offset or Limit go out of bounds, return a truncated list. If Offset is OOB, that means return an empty list.

There is no real reason that this should return an error.

## Fetching your own score
```
Request: GET /runs/{uuid}
    Empty

Response: 200 OK
    LBEntry

Response: 404 Not Found
    GenericError
```
This endpoint fetches the user's own score from the leaderboard using their UUID to look it up.

If no run is found, return 404.

## Reporting a suspicious score
```
Request: POST /reports/{runid}
    userToken: string
        UUID of the user reporting, for accountability.

    reason: string
        user-provided string containing the reason for the report.

Response: 201 CREATED
    Empty

Response: 404 Not Found
    GenericError

Response: 429 Bad Request
    GenericError
```
This endpoint attaches a report to a run. The runid is the ID of the run being reported. As in the same ID as what
appears in a LBEntry.

If the runid is not found, return 404.

If the reason is empty or missing, or the userToken is missing, return 429.

## Submitting a run
```
Request: POST /runs
    SubmitRunData

Response: 201 CREATED
    Empty

Response: 400
    GenericError

Response: 429 BAD REQUEST
    GenericError

```
This endpoint accepts a run and adds it to the leaderboard under the user's UUID.

If the UUID doesn't exist yet, create it.

If the new score is not a PB (score > old score), throw it away.

If the run fails to validate, return 429

The server should implement an algorithm to determine whether the player
is definitely cheating. The run data includes a snapshot for every 10
seconds of the run. From there, just read the source code of the game and
check against things that aren't possible.

## Changing your name
```
Request: PATCH /users/{uuid}
    name: string
        the new name to use for this uuid

Response: 200 OK
    Empty

Response: 429 BAD REQUEST
    GenericError
```
The user uses this to change their leaderboard name. If the name is empty or non-existent, return 429.

Note: the client expects name characters to be in the range 0x20..0x7E and operates under that assumption. The client
also truncates names that are longer than 30 characters.
