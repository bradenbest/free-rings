/*
 * Input - Manages input events that aren't mouse or keyboard related
 *
 * Static Methods
 *     init()
 *         called by Main.init_all(), calls Mouse and Keyboard init
 *
 *     addev(eventname, eventfn)
 *         registers an event to the main canvas
 *
 *     ev_blur(ev)
 *         pauses the game and resets the mouse
 *
 *     ev_focus(ev)
 *         unpauses the game
 *
 * Events
 *     The ev functions are named after the events they listen for.
 *     See MDN for information about events
 */

class Input {
    static init() {
        Input.addev("blur",  Input.ev_blur);
        Input.addev("focus", Input.ev_focus);
    }

    /* 1. avoid duplicate event listeners. Removing events that aren't
     *    registered seems to work just fine
     */
    static addev(eventname, eventfn) {
        Draw.CANVAS.removeEventListener(eventname, eventfn); // 1
        Draw.CANVAS.addEventListener(eventname, eventfn);
    }

    static ev_blur(ev) {
        Game.pause();
        Mouse.lb = 0;
        Mouse.put_held_ring_back();
        Console.log("Game lost focus");
    }

    static ev_focus(ev) {
        Game.unpause();
    }
}
