/*
 * Mouse - Manages mouse events
 *
 * State
 *     x: int
 *         x position of mouse relative to main canvas
 *
 *     y: int
 *         y position of mouse relative to main canvas
 *
 *     lb: bool
 *         left button state (true/1 = down, false/0 = not down)
 *
 *     heldring: ringtype (int)
 *         type of ring being held, -1 = invalid/none
 *
 *     heldringstack: stackid (int)
 *         stack where heldring came from, -1 = invalid/none
 *
 *     curstack: stackid (int)
 *         stack mouse is currently hovering over
 *
 * Static Methods
 *     init()
 *         called by Input.init()
 *
 *     set_coords(ev)
 *         sets mouse coordinates to event coordinates relative to
 *         main canvas
 *
 *     put_held_ring_back()
 *         nullifies the ring the mouse is holding and puts it back where
 *         it came from, even if that means a game over
 *
 *     ev_mouseleave(ev)
 *         resets mouse
 *
 *     ev_mousedown(ev)
 *         only runs if left button is down.
 *         If game is paused, unpauses the game.
 *         Updates mouse coords, sets left button flag, checks which
 *         stack the mouse is over. If it's the center stack, the ring is
 *         sent as if the player had pressed space, otherwise, the ring
 *         is popped off its stack and attached to the mouse
 *
 *     ev_mouseup(ev)
 *         resets left button flag, checks if the mouse is actually
 *         holding a valid ring. If not, then nothing is done. Else...
 *         updates the mouse coordinates, checks which stack the mouse is
 *         over if it's not over a stack or is over the center stack, the
 *         ring is put back where it came from (put_held_ring_back). If
 *         the stack has a smaller ring on top than what is being held,
 *         the move is invalid, and the ring is also put back.
 *         Otherwise, the ring is put down on the stack the mouse is
 *         hovering over, and is removed from the mouse
 *
 *     ev_mousemove(ev)
 *         updates mouse coordinates and which stack the mouse is
 *         hovering over
 */

class Mouse {
    static x;
    static y;
    static lb;
    static heldring;
    static heldringstack;
    static curstack;

    static init() {
        Mouse.x = 0;
        Mouse.y = 0;
        Mouse.lb = 0;
        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
        Mouse.curstack = -1;
        Input.addev("mousemove",  Mouse.ev_mousemove);
        Input.addev("mousedown",  Mouse.ev_mousedown);
        Input.addev("mouseup",    Mouse.ev_mouseup);
        Input.addev("mouseleave", Mouse.ev_mouseleave);
    }

    static get_coords() {
        return [Mouse.x, Mouse.y];
    }

    static set_coords(ev) {
        if (ev.type === "phony") {
            Mouse.x = ev.x;
            Mouse.y = ev.y;
            return;
        }

        Mouse.x = ev.pageX - ev.currentTarget.offsetLeft;
        Mouse.y = ev.pageY - ev.currentTarget.offsetTop;
    }

    static lmb_down() {
        return Mouse.lb == 1;
    }

    static put_held_ring_back() {
        if (typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
            return;

        Stack.push(Mouse.heldringstack, Mouse.heldring);
        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
    }

    /* This function prevents you from dragging a ring outside of the
     * canvas. This may cause a couple game overs, but if the ring is
     * allowed to be dragged out of the canvas, then it creates a bug
     * whereing the player can cheat by "deleting" rings that are
     * inconvenient for them. Drag ring out, let go of mouse, mouseup
     * event never fires, then bring the ring to a stack and put it down,
     * it vanishes out of existence. This happens because when the
     * mouseup event is unable to fire, the game thinks you are in a
     * mouse down state, but since you are unable to fire a mouseup event
     * as the mouse button is not down, the only thing to do is trigger
     * another mousedown event, which will grab whatever is at the top of
     * the stack the mouse is over, so the ring is lost. As I have no
     * control over this, the next best thing is to just put the ring
     * back and cancel the mouse down state.
     *
     * For now, this function stays.
     */
    static ev_mouseleave(ev) {
        Mouse.lb = 0;
        Mouse.put_held_ring_back();
    }

    static ev_mousedown(ev) {
        let stackid;

        if (ev.button !== 0)
            return;

        if (Game.paused) {
            Game.unpause();
            return;
        }

        Mouse.set_coords(ev);
        Mouse.lb = 1;
        stackid = Stack.which_stack(Mouse.x, Mouse.y);

        if (stackid === -1)
            return;

        if (stackid === 6){
            Stack.send_ring();
            return;
        }

        Mouse.heldringstack = stackid;
        Mouse.heldring = Stack.pop(stackid);
    }

    static ev_mouseup(ev) {
        let stackid;
        let stackcp;

        Mouse.lb = 0;

        if (typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
            return;

        Mouse.set_coords(ev);
        stackid = Stack.which_stack(Mouse.x, Mouse.y);

        if (stackid === -1 || stackid === 6) {
            Mouse.put_held_ring_back();
            return;
        }

        stackcp = Stack.get_stack(stackid);

        if (stackcp.length > 0 && stackcp.pop() >= Mouse.heldring) {
            Mouse.put_held_ring_back();
            return;
        }

        Stack.push(stackid, Mouse.heldring);
        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
    }

    static ev_mousemove(ev) {
        Mouse.set_coords(ev);
        Mouse.curstack = Stack.which_stack(Mouse.x, Mouse.y);
    }
}
