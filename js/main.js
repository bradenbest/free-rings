/*
 * Main - entry point
 *
 * Static methods
 *     main()
 *         entry point
 *
 *     init_all()
 *         initializes all other modules, focuses the canvas and runs
 *         tests. Called every time the game starts.
 */

class Main {
    static main(){
        Main.init_all();
        Console.log(`Free Rings v${Game.VERSION}`);
        UIMenu.show_title_menu();
    }

    static init_all() {
        const modules = [
            Console,
            Util,
            Input,
            Mouse,
            Keyboard,
            Joypad,
            Stack,
            Draw,
            Animation,
            Game,
            Anticheat,
            Leaderboard,
            UIMenu,
            Test,
        ];

        modules.forEach(module => {
            if (typeof module.init === "function")
                module.init();
        });

        Draw.CANVAS.focus();

        if (Game.VERSION.match("test") !== null)
            Test.run();
    }
}
