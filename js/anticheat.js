/*
 * Anticheat - This should stop 90% of cheaters from doctoring scores.
 *             Can't stop all of them, but this is a start
 *
 * State
 *     interval: interval
 *         return value of setInterval, necessary for clearInterval
 *
 *     shadow: array<any>
 *         shadowing for in-game variables
 *
 *     shadow_flag: bool
 *         flag that tracks variable shadowing
 *
 *     start_timestamp: timestamp (int)
 *         timestamp of beginning of game
 *         (currently unused)
 *
 *     end_timestamp: timestamp (int)
 *         timestamp of ending of game
 *
 *     slices: array<object>
 *         array of snapshots of the game state. Used for validation and
 *         as part of the submitted run.
 *
 * Static Methods
 *     init()
 *         called by Main.init_all
 *
 *     make_slice()
 *         adds a slice, which has the score, round, clears and timestamp
 *
 *     validate_run() -> bool
 *         returns true if run appears valid, false if stats are
 *         definitely impossible
 *
 *     detect_cheat() -> bool
 *         returns true if tampering is detected, false otherwise
 *
 *     check_shadow(shadow) -> bool
 *         returns true if shadow is tripped
 *
 *     loop()
 *         runs independently of gameloop, runs anticheat checks on a
 *         500ms interval
 *
 *     kill()
 *         kills anticheat loop
 */

class Anticheat {
    static interval;
    static shadow;
    static shadow_flag;
    static start_timestamp;
    static end_timestamp;
    static slices;

    static init() {
        if (typeof Anticheat.interval !== "undefined")
            clearInterval(Anticheat.interval);

        Anticheat.interval = setInterval(Anticheat.loop, 500);
        Anticheat.shadow = [];
        Anticheat.shadow_flag = false;
        Anticheat.start_timestamp = 0;
        Anticheat.end_timestamp = 0;
        Anticheat.slices = [];
    }

    static make_slice() {
        Anticheat.slices.push({
            score: Game.score,
            round: Game.current_round,
            clears: Game.stacks_cleared,
            timeStamp: Date.now(),
        });
    }

    /* always returns true. There was an algorithm here, but LeMorrow
     * was concerned that cheaters would reverse engineer it and use it
     * to get past the server's checks
     */
    static validate_run() {
        return true;
    }

    /* 1. forgive if it trips on the first frame. This false positive can
     *    happen if the game is refreshed from another browser tab and
     *    then navigated to.
     */
    static detect_cheat() {
        if (Game.tickno < 2) // 1
            return false;

        if (typeof $0 !== "undefined") {
            Console.info("anticheat detected inspect element usage");
            return true;
        }

        if (Game.TEXTAREA.value !== Game.get_stats_info()) {
            Console.info("anticheat detected tampering");
            return true;
        }

        if (Anticheat.shadow_flag) {
            Console.info("anticheat detected tampering");
            return true;
        }

        return false;
    }

    static check_shadow(shadow) {
        if (shadow.pop() !== Game.time_elapsed)
            return true;

        if (shadow.pop() !== Game.score)
            return true;

        if (shadow.pop() !== Game.stacks_cleared)
            return true;

        if (shadow.pop() !== Game.current_round)
            return true;

        if (shadow.pop() !== Game.tickno)
            return true;

        return false;
    }

    static loop() {
        const notif = [
            "Sorry for any inconvenience! You are seeing this because the anticheat has been tripped.",
            "You're not in trouble, but your score won't be uploaded to the leaderboard.",
            "To fix this, restart the game and the 'cheats suspected' flag will go away.",
            "",
            "If you received this in error, please let me know by <a href='https://gitlab.com/bradenbest/free-rings/issues/new'>filing an issue</a>.",
            "Please give steps to reproduce the false positive or else I can't do anything about it.",
        ].join("<br/>");

        if (Anticheat.detect_cheat() && Game.cheats_suspected === false) {
            UIMenu.alert(notif, "I understand");
            Game.cheats_suspected = true;
            Game.TEXTAREA.value = Game.get_stats_info();
        }
    }

    static kill() {
        clearInterval(Anticheat.interval);
    }
}
