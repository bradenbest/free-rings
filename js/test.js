/*
 * Test - miscellaneous playtesting/debugging code. Only runs if
 *        Game.VERSION contains "test". The name is a bit misleading, as
 *        these are not unit tests.
 *
 * Constants
 *     TESTS: array<string>
 *         array of debug function names that are valid
 *
 *     TESTQUEUE: array<string>
 *         array of debug function names that will be run. See Test.TESTS
 *
 * Static Methods
 *     run()
 *         run all tests, logging each function's name in the console
 *
 *     fast_round_transition()
 *         sets game round to 9 for testing round transitions
 *
 *     instant_gameover()
 *         instantly game overs for testing the game over screen
 *
 *     endgame()
 *         sets round and timeout to endgame
 *
 *     trip_anticheat()
 *         Forces the anticheat to trip
 */

class Test {
    static TESTS = [
        "instant_gameover",
        "fast_round_transition",
        "endgame",
        "trip_anticheat",
    ];
    static TESTQUEUE = [
        // empty
    ];

    static run(){
        Test.TESTQUEUE
            .filter(fname => Test.TESTS.includes(fname))
            .forEach(fname => {
                Console.warn(`Test: ${fname}`);
                Test[fname]();
            });
    }

    static fast_round_transition() {
        Game.stacks_cleared = 9;
    }

    static instant_gameover() {
        Stack.push(0, 0);
        Stack.push(0, 0);
    }

    static endgame() {
        Game.current_round = 8;
        Animation.ring_timer_timeout = 1000;
    }

    static trip_anticheat() {
        Anticheat.shadow_flag = 1;
        Game.tickno = 2;
        Anticheat.detect_cheat();
    }
}
