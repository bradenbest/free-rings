/*
 * Console - wrapper class for build-in console functions
 *
 * Constants
 *     TEXTAREA: DOM Element
 *         reference to container for log messages
 *
 * State
 *     isvisible: bool
 *         tracks whether TEXTAREA is visible
 *
 * Static methods
 *     init()
 *         called by Main.init_all()
 *
 *     toggle()
 *         toggles TEXTAREA visibility
 *
 *     log_impl(message, type)
 *         logs a message of type type to TEXTAREA
 *
 *     log(message)
 *         wraps console.log
 *
 *     info(message)
 *         wraps console.info
 *
 *     warn(message)
 *         wraps console.warn
 *
 *     error(message)
 *         wraps console.error
 */

class Console {
    static TEXTAREA;
    static isvisible;

    static init() {
        Console.TEXTAREA = document.querySelector(".console");
        Console.isvisible = false;
    }

    static toggle() {
        Console.isvisible = !Console.isvisible;

        if (Console.isvisible)
            Console.TEXTAREA.style.display = "block";
        else
            Console.TEXTAREA.style.display = "none";
    }

    static log_impl(message, type) {
        let container = document.createElement("p");

        container.className = type;
        container.innerHTML = message.toString();
        Console.TEXTAREA.appendChild(container);
        Console.TEXTAREA.scrollTop += container.offsetHeight;
    }

    static log(message) {
        console.log(message);
        Console.log_impl(message, "log");
    }

    static info(message) {
        console.info(message);
        Console.log_impl(message, "info");
    }

    static warn(message) {
        console.warn(message);
        Console.log_impl(message, "warn");
    }

    static error(message) {
        console.error(message);
        Console.log_impl(message, "error");
    }
}
