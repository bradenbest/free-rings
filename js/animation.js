/*
 * Animation - manages stateful animations without making a mess in Game
 *
 * Constants
 *     TYPES: map<string, int>
 *         enumerates the animation types
 *
 *     ZINDEX: map<string, int>
 *         used to sort animations in queue for drawing order
 *
 *     STEP_FNS: map<string, fn -> bool>
 *         used to map animation types to their animation step functions
 *
 *     REQUIRED_FIELDS: array<string>
 *         used to sanity check Animation constructor
 *
 *     RING_TIMER_LOW_CAP: ms (int)
 *         ring_timer_timeout cannot go lower than this
 *
 *     RING_TIMER_DECAY: ms (int)
 *         how much ring_timer_timeout decreases by
 *
 * State
 *     list: array<Animation>
 *         Z-sorted queue holding active animations
 *
 *     ring_timer_timeout: ms (int)
 *         used to track how long ring_timer should run for and speeds
 *         up throughout the game
 *
 * Instance
 *     type: string
 *         Generic, Mandatory
 *         The type of animation (see Animation.TYPES).
 *
 *     starttime: timestamp
 *         Generic, Mandatory
 *         when the animation started. Pausing bumps this up to now.
 *
 *     callback: fn
 *         Generic, Optional
 *         called when an animation goes inactive (see instance.kill())
 *
 *     lasttime: timestamp
 *         Generic, Automatically set
 *         timestamp of last animation step
 *
 *     stepfn: fn -> bool
 *         Generic, Automatically set
 *         called on every frame until the animation is inactive
 *         returns true or false to indicate whether animation is still
 *         active
 *
 *     zindex: int
 *         Generic, Automatically set
 *         used to sort animation in queue
 *
 *     is_active: bool
 *         Generic, Automatically set
 *         used to filter animations out of the queue
 *
 *     interrupt: bool
 *         Generic, Externally set
 *         used to stop animations early. Currently only used with
 *         ring_timer
 *
 *     src: stackid (int)
 *         ring_move
 *         source ring stack, where ring is coming from
 *
 *     dest: stackid (int)
 *         ring_move
 *         destination ring stack, where ring is going
 *
 *     ring: ringtype (int)
 *         ring_move
 *         type of ring being moved
 *
 *     selstack: stackid (int)
 *         ring_timer
 *         stack where ring will be sent (see also ring_move/dest)
 *
 *     selring: ringtype (int)
 *         ring_timer
 *         ring that will be sent (see also ring_move/ring)
 *
 *     timeout: ms (int)
 *         ring_timer
 *         length of countdown
 *
 *     roundno: int
 *         new_round
 *         round to be displayed
 *
 * Static Methods
 *     init()
 *         called by Main.init_all()
 *
 *     step_ring_move(selanim, curstamp) -> bool
 *         lerps ring from center to target stack
 *
 *     step_ring_timer(selanim, curstamp) -> bool
 *         performs ring countdown
 *
 *     step_ring_init(selanim, curstamp) -> bool
 *         bootstraps a ring_timer animation and immediately goes
 *         inactive
 *
 *     step_new_round(selanim, curstamp) -> bool
 *         displays new round text for 2 seconds
 *
 *     callback_ring_move(selanim)
 *         creates a ring_init animation as part of the bootstrapping
 *         procedure, also does the actual moving of the ring
 *
 *     callback_ring_timer(selanim)
 *         creates a ring_move animation and decays the timeout
 *
 *     push(anim)
 *         sorted_insert into queue using zindex as the cmp rule
 *
 *     step_all(curstamp)
 *         calls the step function of every active animation in the
 *         queue, and then removes any inactive animations
 *
 *     restart_animations()
 *         restarts all animations in the queue (see also restart
 *         Instance method)
 *
 *     get_ring_timer_animation() -> Animation
 *         gets the ring_timer animation from the queue, or returns null
 *         if there are zero or more than one. This is necessary to as
 *         the gameloop has to render the line_to_stack before the
 *         animation as a matter of visual polish.
 *
 *
 * step_* functions
 *     selanim: Animation
 *         an animation from the queue
 *
 *     curstamp: timestamp
 *         timestamp from gameloop
 *
 *     Return Value
 *         indicates whether animation is still active
 *
 *     Description
 *         these are called once per frame and represent a step of
 *         animation
 *
 * callback_* functions
 *     selanim: Animation
 *         an animation from the queue
 *
 *     Description
 *         these are called when an animation becomes inactive, and
 *         should only be called once. I already fixed a bug where
 *         ring_move's callback was erroneously being called twice
 *
 * Instance Methods
 *     constructor(obj)
 *         see Instance documentation for fields that can go in obj
 *
 *     kill()
 *         sets active flag to false and calls the callback if it exists
 *
 *     restart()
 *         sets starttime to now so that the animation starts over
 *
 */

class Animation {
    static TYPES = {
        "ring_move":  0,
        "ring_timer": 1,
        "ring_init":  2,
        "new_round":  3,
    };
    static ZINDEX = {
        "ring_move":  2,
        "ring_timer": 1,
        "ring_init":  0,
        "new_round":  3,
    };
    static STEP_FNS = {
        "ring_move":  Animation.step_ring_move,
        "ring_timer": Animation.step_ring_timer,
        "ring_init":  Animation.step_ring_init,
        "new_round":  Animation.step_new_round,
    };
    static REQUIRED_FIELDS = ["type", "starttime"];
    static RING_TIMER_LOW_CAP = 1000;
    static RING_TIMER_DECAY = 30;
    static list;
    static ring_timer_timeout;

    // generic
    type      = null;
    starttime = null;
    callback  = null;
    lasttime  = null;
    stepfn    = null;
    zindex    = null;
    is_active = null;
    interrupt = null;

    // ring_move
    src       = null;
    dest      = null;
    ring      = null;

    // ring_timer
    selstack  = null;
    selring   = null;
    timeout   = null;

    // new_round
    roundno   = null;

    static init() {
        Animation.list = [];
        Animation.ring_timer_timeout = 10000;
    }

    constructor(obj = {}) {
        const selfref = this;

        if (!(obj.type in Animation.TYPES)) {
            Console.error(`invalid animation type '${obj.type}'`);
            return;
        }

        Object.keys(obj).forEach(key => {
            selfref[key] = obj[key];
        });

        if (Animation.REQUIRED_FIELDS.filter(field => selfref[field] === null).length > 0)
            throw "Animation: missing required field";

        this.lasttime = this.starttime;
        this.stepfn = Animation.STEP_FNS[this.type];
        this.zindex = Animation.ZINDEX[this.type];
        this.is_active = true;
    }

    kill() {
        this.is_active = false;

        if (typeof this.callback === "function")
            this.callback(this);
    }

    restart() {
        this.starttime = Game.laststamp;
    }

    /* Linear Interpolation: A + (t * (B - A) / resolution)
     */
    static step_ring_move(selanim, curstamp) {
        const resolution = 100;
        let diff = curstamp - selanim.starttime;
        let step = diff;
        let cs = Stack.get_coords(selanim.src);
        let ce = Stack.get_coords(selanim.dest);
        let outx = cs[0] + (step * (ce[0] - cs[0]) / resolution);
        let outy = cs[1] + (step * (ce[1] - cs[1]) / resolution);

        Draw.draw_ring_transparent(outx, outy, selanim.ring);
        selanim.lasttime = curstamp;
        return step < resolution;
    }

    /* line_to_stack was moved out of the animation and into Game.loop so
     * that it can be below the ring stacks in the rendering order
     */
    static step_ring_timer(selanim, curstamp) {
        let diff;
        let text;
        let centerxy;

        if (selanim.interrupt)
            return false;

        diff = curstamp - selanim.starttime;
        text = Math.floor((selanim.timeout - diff) / 1000);
        centerxy = Stack.get_coords(6);
        Draw.draw_ring(centerxy[0], centerxy[1], selanim.selring);
        // 1
        Draw.center_text(text + 1);
        selanim.lasttime = curstamp;
        return diff < selanim.timeout;
    }

    static step_ring_init(selanim, curstamp) {
        let selstack = Game.random_stack();
        let selring = Game.random_ring();

        Stack.push(6, selring);

        Animation.push(new Animation({
            type:      "ring_timer",
            starttime: curstamp,
            selstack:  selstack,
            selring:   selring,
            timeout:   Animation.ring_timer_timeout,
            callback:  Animation.callback_ring_timer,
        }));

        return false;
    }

    static step_new_round(selanim, curstamp) {
        let diff = curstamp - selanim.starttime;
        selanim.lasttime = curstamp;
        Draw.draw_round_text(selanim.roundno);
        return diff < 2000;
    }

    static callback_ring_move(selanim) {
        Stack.push(selanim.dest, selanim.ring);

        Animation.push(new Animation({
            type:      "ring_init",
            starttime: selanim.lasttime,
        }));
    }

    static callback_ring_timer(selanim) {
        let stackdest = selanim.selstack;
        let centerring = selanim.selring;

        if (Animation.ring_timer_timeout > Animation.RING_TIMER_LOW_CAP)
            Animation.ring_timer_timeout -= Animation.RING_TIMER_DECAY;

        Animation.push(new Animation({
            type:      "ring_move",
            starttime: selanim.lasttime,
            src:       6,
            dest:      stackdest,
            ring:      centerring,
            callback:  Animation.callback_ring_move,
        }));
    }

    static push(anim) {
        const zindexcmp = (a, b) => a.zindex - b.zindex;

        Util.sorted_insert(Animation.list, anim, zindexcmp);
    }

    /* 1. The first filter fixes a bug where early-sending a ring during
     *    round transition insta-kills the player because the ring_move
     *    callback somehow gets called twice. The second filter is just
     *    proper memory management. Not removing the reference would be
     *    a memory leak
     */
    static step_all(curstamp) {
        Animation.list
            .filter(anim => anim.is_active) // 1
            .forEach(anim => {
                if (anim.stepfn(anim, curstamp) === false)
                    anim.kill();
            });

        Animation.list = Animation.list.filter(anim => anim.is_active); // 1
    }

    static restart_animations() {
        Animation.list.forEach(anim => {
            anim.restart();
        });
    }

    static get_ring_timer_animation() {
        let flist = Animation.list.filter(anim => anim.type === "ring_timer");

        if (flist.length !== 1)
            return null;

        return flist.pop();
    }
}
