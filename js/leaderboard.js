/*
 * Leaderboard - manages leaderboard UI and connection
 *
 * Constants
 *     API_ENDPOINT: string
 *         base URL of API endpoint
 *
 *     GENERIC_HEADERS: map<string, string>
 *         generic HTTP headers
 *
 *     LOCALSTORAGE_KEY: string
 *         storage key for leaderboard data that needs to be stored
 *
 *     LSDATA_DEFAULT: string
 *         JSON string containing default parameters for local storage
 *
 * State
 *     lsdata: object
 *         contains local storage data
 *         uuid: string
 *             player UUID
 *
 *         game_version: string
 *             last game version used
 *
 *     isvisible: bool
 *         tracks whether the leaderboard UI is visible
 *
 * Static methods
 *     init()
 *         called by Main.init_all()
 *
 *     load_lsdata()
 *         loads data from localstorage into Leaderboard.lsdata
 *
 *     save_lsdata()
 *         saves data to localstorage
 *
 *     generate_uuid() -> string
 *         generates a UUID for the player
 *
 *     show_leaderboard()
 *         called by Keyboard binding (L)
 *         shows the leaderboard if the game loop is not running
 *
 *     get_leaderboard(offset, limit) -> object
 *         fetches a leaderboard page from the server
 *
 *     get_own_score() -> object
 *         fetches self score from server
 *
 *     report_run(runid, reason) -> bool
 *         attaches a report to a run in the leaderboard
 *
 *     set_name(newname) -> bool
 *         changes the user's name on the leaderboard
 *
 *     submit_run() -> object
 *         submits a run to the leaderboard, returns server response or
 *         null if it failed
 */

class Leaderboard {
    static API_ENDPOINT = "localhost";
    static GENERIC_HEADERS = {
        "accept":       "application/json",
        "Content-Type": "application/json",
    }
    static LOCALSTORAGE_KEY = "free-rings-leaderboard";
    static LSDATA_DEFAULT = `{"uuid": null, "game_version": null}`;
    static lsdata;
    static isvisible;

    static init() {
        Leaderboard.load_lsdata();

        if (Leaderboard.lsdata.uuid === null) {
            Leaderboard.lsdata.uuid = Leaderboard.generate_uuid();
            Leaderboard.save_lsdata();
        }

        Leaderboard.lsdata.game_version = Game.VERSION;
        Leaderboard.save_lsdata();
        Leaderboard.isvisible = false;
    }

    static load_lsdata() {
        let lsdata_str = localStorage.getItem(Leaderboard.LOCALSTORAGE_KEY);

        if (lsdata_str === null)
            lsdata_str = Leaderboard.LSDATA_DEFAULT;

        Leaderboard.lsdata = JSON.parse(lsdata_str);
    }

    static save_lsdata() {
        localStorage.setItem(Leaderboard.LOCALSTORAGE_KEY, JSON.stringify(Leaderboard.lsdata));
    }

    static generate_uuid() {
        if (typeof crypto.randomUUID !== "function") {
            Console.error("crypto.randomUUID is unavailable. Please update your browser.");
            Console.warn("Leaderboard: using fallback `generate_uuid`");
            return Util.generate_uuid();
        }

        return crypto.randomUUID();
    }

    static show_leaderboard() {
        if (Game.looprunning)
            return;

        UIMenu.show_leaderboard_menu();
    }

    static async get_leaderboard(offset, limit) {
        const reqbody = {
            Offset: offset,
            Limit:  limit,
        };
        const req = {
            method: "GET",
            headers: Leaderboard.GENERIC_HEADERS,
        };
        let response = await Util.fetch(Leaderboard.API_ENDPOINT + "/runs" + Util.obj_to_search(reqbody));
        let rescontents = await response.json();

        if (!response.ok) {
            Console.error(`Leaderboard.get_leaderboard: got ${response.status}: ${rescontents.message}`);
            return {total: 0, runs: []};
        }

        return rescontents;
    }

    static async get_own_score() {
        let response = await Util.fetch(Leaderboard.API_ENDPOINT + "/runs/" + Leaderboard.lsdata.uuid);
        let rescontents = await response.json();

        if (!response.ok) {
            Console.error(`Leaderboard.get_own_score: got ${response.status}: ${rescontents.message}`);
            return {
                "id": -1,
                "position": "N/A",
                "name": "N/A",
                "score": "N/A",
                "startTimestamp": "N/A",
                "endTimestamp": "N/A",
                "round": "N/A",
                "clears": "N/A",
            }
        }

        return rescontents;
    }

    static async report_run(runid, reason) {
        const reqbody = {
            userToken: Leaderboard.lsdata.uuid,
            reason:    reason,
        };
        const req = {
            method:  "POST",
            headers: Leaderboard.GENERIC_HEADERS,
            body:    JSON.stringify(reqbody),
        };
        let response;
        let rescontents;

        if (runid < 0 || reason === null)
            return false;

        response = await Util.fetch(Leaderboard.API_ENDPOINT + "/reports/" + runid, req);
        rescontents = await response.json();

        if (!response.ok) {
            Console.error(`Leaderboard.report_run: got ${response.status}: ${rescontents.message}`);
            return false;
        }

        Console.log("Thanks for the report. We'll look into it.");
        return true;
    }

    static async set_name(newname) {
        const reqbody = {
            name: newname,
        };
        const req = {
            method:  "PATCH",
            headers: Leaderboard.GENERIC_HEADERS,
            body:    JSON.stringify(reqbody),
        };
        let response = await Util.fetch(Leaderboard.API_ENDPOINT + "/users/" + Leaderboard.lsdata.uuid, req);
        let rescontents = await response.json();

        if (!response.ok) {
            Console.error(`Leaderboard.set_name: got ${response.status}: ${rescontents.message}`);
            return false;
        }

        return true;
    }

    static async submit_run() {
        const reqbody = {
            userToken: Leaderboard.lsdata.uuid,
            score:     Game.score,
            clears:    Game.stacks_cleared,
            round:     Game.current_round,
            startTime: (Anticheat.end_timestamp - Game.time_elapsed)|0,
            endTime:   Anticheat.end_timestamp|0,
            slices:    Anticheat.slices,
        };
        const req = {
            method:  "POST",
            headers: Leaderboard.GENERIC_HEADERS,
            body:    JSON.stringify(reqbody),
        };
        let response = await Util.fetch(Leaderboard.API_ENDPOINT + "/runs", req);
        let rescontents = await response.json();

        if (!response.ok) {
            Console.error(`Leaderboard.submit_run: got ${response.status}: ${rescontents.message}`);
            return null;
        }

        return rescontents;
    }
}
