/*
 * Joypad - manages gamepad
 *
 * Maintaining this is tedious because the Gamepad API has been locked
 * down to secure contexts, so testing it locally is impossible. I have
 * to commit it and upload it before I can actually test it, which I find
 * incredibly obnoxious. This module will likely cause the most version
 * bumps.
 *
 * This module remembers the first controller that was connected and will
 * only respond to that controller until it is disconnected
 *
 * Constants
 *     BUTTON_DOWN_CALLBACKS: map<int, fn>
 *         callbacks for gamepad buttons when going from unpressed to
 *         pressed
 *
 *     BUTTON_UP_CALLBACKS: map<int, fn>
 *         callbacks for gamepad buttons when going from pressed to
 *         unpressed
 *
 *     AXIS_THRESHOLD: float
 *         how far the axis (left thumbstick) has to be pushed out in a
 *         direction before it's considered "in" that direction
 *         an axis in API terms is a number between -1 and +1.
 *
 * State
 *     gamepad_ref: Gamepad or null
 *         reference to active gamepad
 *
 *     button_states: map<int, bool>
 *         registered buttons and their states (false = up, true = down)
 *
 *     selstack: int
 *         selected stack (see stack.js)
 *
 *     available: bool
 *         whether the API is available. If it's not, it's because the
 *         game was loaded in an insecure context.
 *
 * Static Methods
 *     init()
 *         called by Main.init_all
 *
 *     get_gamepad() -> Gamepad or null
 *         returns gamepad reference or null
 *
 *     ev_gamepadconnected(ev)
 *     ev_gamepaddisconnected(ev)
 *         event handlers
 *
 *     button_0_down()
 *     button_2_down()
 *     button_9_down()
 *         called when button is pressed
 *
 *     button_0_up()
 *         called when button is released
 *
 *     generate_phony_event_data() -> {type: "phony", x: int, y: int}
 *         generates a phony event {type, x, y} based on the direction
 *         of the L Axis
 *
 *     poll_buttons()
 *         polls registered buttons and whether their state has changed
 *         if so, the relevant handler is called (BUTTON_DOWN_CALLBACKS,
 *         BUTTON_UP_CALLBACKS)
 *
 *     poll_axes()
 *         polls the left axis (Left thumbstick) and selects a stack
 *         based on its direction. The stack's coordinates are used in
 *         button_0_down() and button_0_up() to fire phony mouse events
 */
class Joypad {
    static BUTTON_DOWN_CALLBACKS = {
        0: Joypad.button_0_down, // A
        2: Joypad.button_2_down, // X
        9: Joypad.button_9_down, // Start
    };
    static BUTTON_UP_CALLBACKS = {
        0: Joypad.button_0_up,
    };
    static AXIS_THRESHOLD = 0.55;
    static gamepad_ref;
    static button_states;
    static selstack;
    static available;

    static init() {
        Joypad.gamepad_ref = null;
        Joypad.button_states = [];
        Joypad.selstack = -1;
        Joypad.available = true;

        if (typeof navigator.getGamepads === "undefined") {
            Console.warn("Gamepad unavailable");
            Joypad.available = false;
            return;
        }

        addEventListener("gamepadconnected", Joypad.ev_gamepadconnected);
        addEventListener("gamepaddisconnected", Joypad.ev_gamepaddisconnected);
    }

    // TODO: at some point, I should select joypads by their index and
    // add a UIMenu for changing which joypad is used. But that point is
    // not now
    static get_gamepad() {
        let gamepad_list;

        if (!Joypad.available || typeof Joypad.gamepad_ref === "undefined")
            return Joypad.gamepad_ref = null;

        if (Joypad.gamepad_ref !== null)
            return Joypad.gamepad_ref;

        gamepad_list = navigator.getGamepads();

        if (gamepad_list.length === 0)
            return null;

        Joypad.gamepad_ref = gamepad_list.pop();
        return Joypad.gamepad_ref;
    }

    static ev_gamepadconnected(ev) {
        if (Joypad.gamepad_ref === null)
            Joypad.gamepad_ref = ev.gamepad;
    }

    static ev_gamepaddisconnected(ev) {
        if (ev.gamepad.index === Joypad.gamepad_ref.index)
            Joypad.gamepad_ref = null;
    }

    static button_0_down() {
        Mouse.ev_mousedown(Joypad.generate_phony_event_data());
    }

    static button_2_down() {
        Stack.send_ring();
    }

    static button_9_down() {
        Game.toggle_pause();
    }

    static button_0_up() {
        Mouse.ev_mouseup(Joypad.generate_phony_event_data());
    }

    static generate_phony_event_data() {
        let coord = Stack.COORDS[Joypad.selstack];

        if (typeof coord === "undefined")
            coord = Stack.COORDS[6];

        return {
            type: "phony",
            button: 0,
            x: coord[0],
            y: coord[1],
        };
    }

    /* 1. since we're polling the joypad every frame, we only care about
     *    when the button state changes
     */
    static poll_buttons() {
        let gamepad = Joypad.get_gamepad();

        if (gamepad === null)
            return;

        gamepad.buttons.forEach((button, idx) => {
            let curstate = Joypad.button_states[idx] || false;
            let newstate = button.pressed;

            // 1
            if (curstate === false && newstate === true) {
                let callback = Joypad.BUTTON_DOWN_CALLBACKS[idx];

                Console.log(`Button ${idx} down`);
                Joypad.button_states[idx] = newstate;

                if (typeof callback === "function")
                    callback();
            }

            // 1
            if (curstate === true && newstate === false) {
                let callback = Joypad.BUTTON_UP_CALLBACKS[idx];

                Console.log(`Button ${idx} up`);
                Joypad.button_states[idx] = newstate;

                if (typeof callback === "function")
                    callback();
            }
        });
    }

    static poll_axes() {
        const threshold = Joypad.AXIS_THRESHOLD;
        let gamepad = Joypad.get_gamepad();

        if (gamepad === null)
            return;

        Joypad.selstack = (_ => {
            if (gamepad === null)
                return -1;

            if (gamepad.axes[1] < -threshold)
                return (gamepad.axes[0] < 0) ? (0) : (1);

            if (gamepad.axes[1] > threshold)
                return (gamepad.axes[0] < 0) ? (4) : (5);

            if (gamepad.axes[0] < -threshold)
                return 2;

            if (gamepad.axes[0] > threshold)
                return 3;

            return 6;
        })();

        if (Joypad.selstack !== -1 && Joypad.selstack !== 6)
            Mouse.ev_mousemove(Joypad.generate_phony_event_data());
    }
}
