/* Solarized palette courtesy Ethan Schoonover https://ethanschoonover.com/solarized/
 *
 * Draw - Manages canvas drawing
 *
 * Constants
 *     CANVAS: HTMLCanvasElement
 *         Main game canvas
 *
 *     CANVAS_RB: HTMLCanvasElement
 *         Random bag preview canvas
 *
 *     CTX_ATTRIB: object
 *         context attributes
 *
 *     CTX: CanvasRenderingContext2D
 *         context for main game canvas
 *
 *     CTX_RB: CanvasRenderingContext2D
 *         context for random bag canvas
 *
 *     WIDTH: int
 *         width of main canvas
 *
 *     HEIGHT: int
 *         height of main canvas
 *
 *     RB_WIDTH: int
 *         width of random bag canvas
 *
 *     RB_HEIGHT: int
 *         height of random bag canvas
 *
 *     THEMES: array<string>
 *         available theme names
 *
 *     RING_THEMES: array<string>
 *         available theme names for rings
 *
 *     THEME: string
 *         default theme
 *
 *     RINGTHEME: string
 *         default ring theme
 *
 *     BGCOLOR_THEMES: map<string, CSS Color (string)>
 *         background of the canvas
 *
 *     BGCOLOR_TRANS_THEMES: map<string, CSS Color (string)>
 *         transparent background (pause, game over)
 *
 *     RING_COLORS_THEMES: map<string, array<CSS Color (string)>>
 *         rings, [0] = biggest, [3] = smallest
 *
 *     RING_PLACEHOLDER_THEMES: map<string, CSS Color (string)>
 *         the ring stack backgrounds
 *
 *     TEXT_COLOR_FG_THEMES: map<string, CSS Color (string)>
 *         foreground text color
 *
 *     LINE_COLOR_THEMES: map<string, array<CSS Color (string)>>
 *         line_to_stack colors, [0] = ring_timer, [1] = mouse drag
 *
 * Pseudo-constants
 *     BGCOLOR: CSS Color (string)
 *         selected from BGCOLOR_THEMES
 *
 *     BGCOLOR_TRANS: CSS Color (string)
 *         selected from BGCOLOR_TRANS_THEMES
 *
 *     RING_COLORS: array<CSS Color (string)>
 *         selected from RING_COLORS_THEMES and RING_PLACEHOLDER_THEMES
 *
 *     TEXT_COLOR_FG: CSS Color (string)
 *         selected from TEXT_COLOR_FG_THEMES
 *
 *     TEXT_COLOR_BG: CSS Color (string)
 *         selected from BGCOLOR_TRANS_THEMES
 *
 *     LINE_COLOR: array<CSS Color (string)>
 *         selected from LINE_COLOR_THEMES
 *
 * State
 *     current_theme: string
 *         tracks the name of the theme
 *
 *     current_ring_theme: string
 *         tracks the name of the ring theme
 *
 * Static Methods
 *     init()
 *         called by Main.init_all()
 *
 *     set_theme(theme)
 *         sets necessary styles for theme and ring theme
 *
 *     set_ring_theme(theme)
 *         sets necessary styles for only ring theme
 *
 *     clear()
 *         clears both main and random bag canvases
 *
 *     draw_transparent_background()
 *         draws transparent background for pause and game over screens
 *
 *     path_circle(x, y, size, fn(ctx) -> any) -> any
 *         creates a circular path and calls fn if it is a function,
 *         returning its result
 *
 *     draw_ring(x, y, ringvalue)
 *         draws a ring at the specified coordinates
 *
 *     draw_ring_transparent(x, y, ringvalue)
 *         draws a ring with a transparent hole. This was stupidly hard
 *
 *     draw_ring_timer_line()
 *         draws line from center stack to ring stack being targeted by
 *         currently active ring_timer animation (see animation.js)
 *
 *     draw_tiny_ring(x, y, ringvalue)
 *         draws a miniature ring in the random bag canvas
 *
 *     draw_random_bag()
 *         draws the contents of the current random bag on the random
 *         bag canvas
 *
 *     line_to_stack(stack1, stack2, coloridx) -> bool
 *         draws a line from stack1 to stack2
 *         coloridx: 0 = ring_timer, 1 = mouse drag.
 *         returns false if either stack is out of bounds
 *         returns true to indicate success
 *
 *     center_text(text)
 *         draws text in the center of the main canvas
 *
 *     draw_round_text(round)
 *         draws background stripe and text displaying the current round
 *
 *     draw_pause_screen()
 *         draws the pause screen
 *
 *     draw_game_over()
 *         draws the game over screen
 *
 *     draw_stacks()
 *         draws the contents of ring stacks 0-5 and their placeholder
 *         background
 *
 *     draw_mouse_ring()
 *         if the mouse is holding a ring, draws the ring at the mouse's
 *         coordinates and draws a line from the stack the ring came from
 *         to the stack the mouse is currently hovering over
 *
 *     draw_joypad_indicator()
 *         draws a small circle where the joypad axis is pointing
 */

/* To add a theme: add the theme information below, just follow the
 * comments, and edit css/theme.css
 */

class Draw {
    static CANVAS = document.querySelector(".canvas.game");
    static CANVAS_RB = document.querySelector(".canvas.randbag");
    static CTX_ATTRIB = {
        alpha: false
    };
    static CTX = Draw.CANVAS.getContext("2d", Draw.CTX_ATTRIB);
    static CTX_RB = Draw.CANVAS_RB.getContext("2d", Draw.CTX_ATTRIB);
    static WIDTH = 500;
    static HEIGHT = 500;
    static RB_WIDTH = 500;
    static RB_HEIGHT = 50;
    // themes must be enumerated here in order to be recognized
    static THEMES = ["light", "dark", "solarized"];
    static RING_THEMES = Draw.THEMES.concat(["protanopia", "deuteranopia", "tritanopia"]);
    static THEME = "dark";
    static RINGTHEME = "dark";

    static BGCOLOR_THEMES = {
        // Background color for the document and canvas
        "light":     "#f7f7ff",
        "dark":      "#161616",
        "solarized": "#002b36",
    };
    static BGCOLOR_TRANS_THEMES = {
        // Transparent background for pause, game over and round transition backing
        "light":     "#f7f7ff80",
        "dark":      "#16161680",
        "solarized": "#002b3680",
    };
    static RING_COLORS_THEMES = {
        // Colors for rings (biggest to smallest)
        "light":     [ "#33ee33", "#ff3333", "#3333ff", "#ff7700" ],
        "dark":      [ "#33ee33", "#ff3333", "#00eeff", "#ffff00" ],
        "solarized": [ "#6c71c4", "#cb4b16", "#268bd2", "#eee8d5" ],

        // Colorblind themes, exclusive to rings
        "protanopia":   [ "#e8ce2f", "#8b7f54", "#a3a3b3", "#2c5bb0" ],
        "deuteranopia": [ "#e8ce2f", "#8b7f54", "#a3a3b3", "#2c5bb0" ],
        "tritanopia":   [ "#75ddee", "#ec4144", "#00656c", "#e5aebc" ],
    };
    static RING_PLACEHOLDER_THEMES = {
        // the background for the ring stacks
        "light":     "#aaaaaa",
        "dark":      "#333333",
        "solarized": "#586e75",
    };
    static TEXT_COLOR_FG_THEMES = {
        // Text color used by the canvas
        "light":     "#000000",
        "dark":      "#ffffff",
        "solarized": "#fdf6e3",
    };

    static LINE_COLOR_THEMES = {
        // color of the lines drawn between stacks.
        // [0] = ring_timer indicator
        // [1] = mouse drag indicator
        "light":     ["#ff0000", "#000000"],
        "dark":      ["#444444", "#ffffff"],
        "solarized": ["#b58900", "#fdf6e3"],
    };
    // these are not constants but are still all caps for backward compatibility
    static BGCOLOR;
    static BGCOLOR_TRANS;
    static RING_COLORS;
    static TEXT_COLOR_FG;
    static TEXT_COLOR_BG;
    static LINE_COLOR;
    static current_theme;
    static current_ring_theme;

    /* 1. From testing, this doesn't seem to be necessary, but it's kept
     *    just in case of browser incompatibilities.
     *    tabIndex makes a DOM element "focusable", so that it can
     *    receive events, like onkeyup
     */
    static init() {
        Draw.CANVAS.width = Draw.WIDTH;
        Draw.CANVAS.height = Draw.HEIGHT;
        Draw.CANVAS_RB.width = Draw.RB_WIDTH;
        Draw.CANVAS_RB.height = Draw.RB_HEIGHT;
        Draw.clear();
        Draw.CANVAS.tabIndex = 0; // 1

        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) {
            Draw.THEME = "light";
            Draw.RINGTHEME = "light";
        }

        Draw.set_theme(Draw.THEME);
    }

    static set_theme(theme) {
        let textinfo = document.querySelector(".info");

        document.body.style.background = Draw.BGCOLOR_THEMES[theme];
        textinfo.style.background = Draw.BGCOLOR_THEMES[theme];
        textinfo.style.color = Draw.TEXT_COLOR_FG_THEMES[theme];
        Draw.BGCOLOR = Draw.BGCOLOR_THEMES[theme];
        Draw.BGCOLOR_TRANS = Draw.BGCOLOR_TRANS_THEMES[theme];
        Draw.RING_COLORS = Draw.RING_COLORS_THEMES[theme].map(v => v);
        Draw.RING_COLORS.push(Draw.RING_PLACEHOLDER_THEMES[theme]);
        Draw.TEXT_COLOR_FG = Draw.TEXT_COLOR_FG_THEMES[theme];
        Draw.TEXT_COLOR_BG = Draw.BGCOLOR_TRANS_THEMES[theme];
        Draw.LINE_COLOR = Draw.LINE_COLOR_THEMES[theme];
        Draw.current_theme = theme;
        Draw.current_ring_theme = theme;
    }

    static set_ring_theme(theme) {
        let placeholder = Draw.RING_COLORS.pop();

        Draw.RING_COLORS = Draw.RING_COLORS_THEMES[theme].map(v => v);
        Draw.RING_COLORS.push(placeholder);
        Draw.current_ring_theme = theme;
    }

    static clear() {
        let oldstyle = Draw.CTX.fillStyle;

        Draw.CTX.fillStyle = Draw.BGCOLOR;
        Draw.CTX.fillRect(0, 0, Draw.WIDTH, Draw.HEIGHT);
        Draw.CTX.fillStyle = oldstyle;
        Draw.CTX_RB.fillStyle = Draw.BGCOLOR;
        Draw.CTX_RB.fillRect(0, 0, Draw.RB_WIDTH, Draw.RB_HEIGHT);
    }

    static draw_transparent_background() {
        let oldstyle = Draw.CTX.fillStyle;

        Draw.CTX.fillStyle = Draw.BGCOLOR_TRANS;
        Draw.CTX.fillRect(0, 0, Draw.WIDTH, Draw.HEIGHT);
        Draw.CTX.fillStyle = oldstyle;
    }

    static path_circle(x, y, size, fn) {
        let res;

        Draw.CTX.beginPath();
        Draw.CTX.arc(x, y, size, 0, Math.PI * 2);

        if (typeof fn === "function")
            res = fn(Draw.CTX);

        Draw.CTX.closePath();
        return res;
    }

    static draw_ring(x, y, ringvalue) {
        Draw.path_circle(x, y, Stack.RING_INNER_SIZE + Stack.RING_SIZES[ringvalue]);
        Draw.CTX.fillStyle = Draw.RING_COLORS[ringvalue];
        Draw.CTX.fill();

        Draw.path_circle(x, y, Stack.RING_INNER_SIZE);
        Draw.CTX.fillStyle = Draw.BGCOLOR;
        Draw.CTX.fill();
    }

    /* Draws the ring with a transparent hole. This function shouldn't
     * work. It only works because of some insane arbitrary clipping
     * rules with paths. This function is used in Draw.draw_mouse_ring
     * and Animation.step_ring_move
     */
    static draw_ring_transparent(x, y, ringvalue) {
        let path = new Path2D();
        let ctx = Draw.CTX;

        path.arc(x, y, Stack.RING_INNER_SIZE, 0, Math.PI * 2);
        path.arc(x, y, Stack.RING_INNER_SIZE + Stack.RING_SIZES[ringvalue], 0, Math.PI * 2);
        ctx.save();
        ctx.clip(path, "evenodd");
        ctx.fillStyle = Draw.RING_COLORS[ringvalue];
        ctx.fill(path);
        ctx.restore();
    }

    static draw_ring_timer_line() {
        let anim = Animation.get_ring_timer_animation();

        if (anim === null)
            return;

        Draw.line_to_stack(6, anim.selstack, 0);
    }

    static draw_tiny_ring(x, y, ringvalue) {
        Draw.CTX_RB.beginPath();
        Draw.CTX_RB.arc(x, y, 5 + Stack.RING_SIZES[6], 0, Math.PI * 2);
        Draw.CTX_RB.fillStyle = Draw.RING_COLORS[ringvalue];
        Draw.CTX_RB.fill();
        Draw.CTX_RB.closePath();

        Draw.CTX_RB.beginPath();
        Draw.CTX_RB.arc(x, y, 5, 0, Math.PI * 2);
        Draw.CTX_RB.fillStyle = Draw.BGCOLOR;
        Draw.CTX_RB.fill();
        Draw.CTX_RB.closePath();
    }

    static draw_random_bag() {
        Game.random_bag.forEach((ring, i) => {
            Draw.draw_tiny_ring((i + 1) * 45, Draw.RB_HEIGHT / 2, ring);
        });
    }

    static line_to_stack(stack1, stack2, coloridx) {
        let stack1xy = Stack.COORDS[stack1];
        let stack2xy = Stack.COORDS[stack2];
        let color = Draw.LINE_COLOR[coloridx];

        if (typeof stack1xy === "undefined" || typeof stack2xy === "undefined")
            return false;

        Draw.CTX.strokeStyle = color;
        Draw.CTX.lineWidth = 5;
        Draw.CTX.lineCap = "round";
        Draw.CTX.beginPath();
        Draw.CTX.moveTo(stack1xy[0], stack1xy[1]);
        Draw.CTX.lineTo(stack2xy[0], stack2xy[1]);
        Draw.CTX.stroke();
        Draw.CTX.closePath();

        return true;
    }

    static center_text(text) {
        let str = text.toString();

        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 30px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText(text, Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_round_text(round) {
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_BG;
        Draw.CTX.fillRect(0, Draw.HEIGHT / 2 - 30, Draw.WIDTH, 60);
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 50px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText(`Round ${round}`, Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_pause_screen() {
        Draw.draw_transparent_background();
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 100px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText("Paused", Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_game_over() {
        Draw.draw_transparent_background();
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 80px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText("GAME", Draw.WIDTH / 2, Draw.HEIGHT / 2 - 30);
        Draw.CTX.fillText("OVER", Draw.WIDTH / 2, Draw.HEIGHT / 2 + 30);
        Draw.CTX.font = "bold 30px monospace";
        Draw.CTX.fillText("Press R to restart", Draw.WIDTH / 2, Draw.HEIGHT / 2 + 90);
        Draw.CTX.fillText("Press L for leaderboards", Draw.WIDTH / 2, Draw.HEIGHT / 2 + 120);
    }

    static draw_stacks() {
        for (let i = 0; i < Stack.NSTACKS - 1; ++i) {
            const stack_pos = Stack.COORDS[i];
            let selstack = Stack.get_stack(i);

            Draw.draw_ring(stack_pos[0], stack_pos[1], 4);

            selstack.forEach(ring => {
                Draw.draw_ring(stack_pos[0], stack_pos[1], ring);
            });
        }
    }

    static draw_mouse_ring() {
        if (typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
            return;

        Draw.draw_ring_transparent(Mouse.x, Mouse.y, Mouse.heldring);
        Draw.line_to_stack(Mouse.heldringstack, Mouse.curstack, 1);
    }

    static draw_joypad_indicator() {
        if (Joypad.selstack === -1 || Joypad.selstack === 6)
            return;

        Draw.CTX.fillStyle = Draw.RING_COLORS[4];
        Draw.CTX.beginPath();
        Draw.CTX.arc(Mouse.x, Mouse.y, 10, 0, 2 * Math.PI);
        Draw.CTX.fill();
        Draw.CTX.closePath();
    }
}
