/*
 * UIMenu - Manages menu UI
 *
 * Constants
 *     LB_PER_PAGE: int
 *         number of leaderboard entries shown per page
 *
 * State
 *     lb_current_offset: int
 *         current leaderboard offset, used by Nav UI
 *
 *     lb_total_runs: int
 *         total runs in leaderboard, returned by server
 *
 *     own_score: object
 *         reference to the user's current PB run. Set by
 *         leaderboard_own_score()
 *
 *     title_active: bool
 *         tracks whether title screen is active so that it can be
 *         reopened if needed
 *
 *     leaderboard_menu_ref: DOM Element
 *         back-reference to leaderboard menu, used for removing it
 *
 *     leaderboard_canceled: bool
 *         tracks whether leaderboard was canceled before it was finished
 *         loading. If set, the loaded leaderboard is not added to the
 *         DOM
 *
 * Static Methods
 *     init()
 *         called by Main.init_all()
 *
 *     show_theme_menu()
 *         shows theme selection menu and pauses the game
 *
 *     show_title_menu()
 *         shows menu
 *
 *     show_leaderboard_menu(offset?)
 *         shows leaderboard menu, replacing it if it already exists
 *         offset detaults to 0
 *
 *     remove_leaderboard_menu()
 *         removes leaderboard menu via leaderboard_menu_ref
 *
 *     theme_menu() -> DOM Element
 *         for show_theme_menu()
 *         creates a theme selection menu and returns the root node
 *
 *     theme_list(menuref, themelistref, setthemefnref) -> DOM Element
 *         for theme_menu()
 *         creates theme list, menuref is a reference to theme_menu root
 *         node, themelistref is a list of theme names, setthemefnref is
 *         a function
 *
 *     title_menu() -> DOM Element
 *         for show_title_menu()
 *         creates a title menu
 *
 *     title_logo() -> DOM Element
 *         for title_menu()
 *         creates a canvas bearing the logo
 *
 *     leaderboard_placeholder() -> DOM Element
 *         for show_leaderboard_menu()
 *         creates leaderboard placeholder for when leaderboard is
 *         loading
 *
 *     leaderboard_menu(offset) -> DOM Element
 *         for show_leaderboard_menu()
 *         creates leaderboard menu at offset
 *
 *     leaderboard_nav(offset, totalruns) -> DOM Element
 *         for leaderboard_menu()
 *         creates navigation UI
 *
 *     leaderboard_entry(lbentry) -> array<DOM Element>
 *         creates a single leaderboard entry, which is an array of table
 *         rows. lbentry is an object as-returned by the leaderboard
 *         server, see SERVER.md for more details
 *
 *     leaderboard_listing(offset) -> DOM Element
 *         for leaderboard_menu()
 *         creates leaderboard listing at offset
 *
 *     leaderboard_own_score() -> DOM Element
 *         for leaderboard_menu()
 *         creates own score section
 *
 *     stringform(prompt_text, callback)
 *         gets string from user and calls callback with response text
 *
 *     alert(prompt_text, okbtn_text)
 *         replaces alert, throws up a modal dialogue box with an ok
 *         button
 *
 * DOM Element factories
 * use when constructing menus.
 *
 *     container(...children) -> DOM Element
 *         creates a .uimenu_container multi_node
 *
 *     multi_node(tagname, ...children) -> DOM Element
 *         creates a generic multi-node container
 *
 *     table_row(...columns) -> DOM Element
 *         creates a table row with columns, if columns are not
 *         HTMLElements, then a UIMenu.text_node(td) is created out of
 *         them.
 *
 *     table_row_hidden(...columns) -> DOM Element
 *         creates a table_row() with display: none
 *
 *     text_node(tagname, text) -> DOM Element
 *         creates a generic text node
 *
 *     section_label(text) -> DOM Element
 *         creates a text_node for labeling sections
 *
 *     input_button(label, callback) -> DOM Element
 *         creates a generic inline button
 *
 *     title_button(label, callback) -> DOM Element
 *         creates an input_button with the CSS block class added
 *
 *     table_button_inline(label, callback) -> DOM Element
 *         creates a <td> containing an input_button
 *
 *     input_text(callback) -> DOM Element
 *         creates a text input field (press enter to call callback)
 *
 *     modal_container(...children) -> DOM Element
 *         for stringform() and alert()
 *         creates container for modals
 *
 *     modal_background() -> DOM Element
 *         for stringform() and alert()
 *         creates modal background
 */

class UIMenu {
    static LB_PER_PAGE = 10;
    static lb_current_offset;
    static lb_total_runs;
    static own_score;
    static title_active;
    static leaderboard_menu_ref;
    static leaderboard_canceled;

    static init() {
        UIMenu.LB_PER_PAGE = 10;
        UIMenu.lb_current_offset = 0;
        UIMenu.lb_total_runs = 0;
        UIMenu.own_score = null;
        UIMenu.title_active = false;
        UIMenu.leaderboard_menu_ref = null;
        UIMenu.leaderboard_canceled = false;
    }

    static show_theme_menu() {
        let menu = UIMenu.theme_menu();

        document.body.appendChild(menu);

        if (Game.looprunning)
            Game.pause();
    }

    static show_title_menu() {
        let menu = UIMenu.title_menu();

        document.body.appendChild(menu);
    }

    static async show_leaderboard_menu(offset) {
        const starting_offset = offset || 0;
        let phony_menu = UIMenu.leaderboard_placeholder();
        let menu;

        if (UIMenu.leaderboard_menu_ref !== null)
            UIMenu.remove_leaderboard_menu();

        document.body.appendChild(phony_menu);
        menu = await UIMenu.leaderboard_menu(starting_offset);
        phony_menu.remove();

        if (UIMenu.leaderboard_canceled) {
            UIMenu.leaderboard_canceled = false;
            return;
        }

        document.body.appendChild(menu);
        UIMenu.leaderboard_menu_ref = menu;
    }

    static remove_leaderboard_menu() {
        UIMenu.leaderboard_menu_ref.remove();
        UIMenu.leaderboard_menu_ref = null;
    }

    static theme_menu() {
        let menu = UIMenu.container();
        let menucontents = UIMenu.multi_node("div",
            UIMenu.section_label("Select a theme by clicking on it."),
            UIMenu.theme_list(menu, Draw.THEMES, Draw.set_theme),
            UIMenu.section_label("Or select a ring theme by clicking on it."),
            UIMenu.theme_list(menu, Draw.RING_THEMES, Draw.set_ring_theme),
        );

        menucontents.className = "uimenu_theme_menu";
        menu.appendChild(menucontents);
        return menu;
    }

    static theme_list(menuref, themelistref, setthemefnref) {
        let listdata = themelistref.map(themename => {
            let item = UIMenu.input_button(themename, ev => {
                setthemefnref(themename);
                Game.unpause();
                menuref.remove();

                if (UIMenu.title_active)
                    UIMenu.show_title_menu();
            });

            return item;
        });
        let listelem = UIMenu.multi_node("div", ...listdata);

        listelem.className = "uimenu_button_list";
        return listelem;
    }

    static title_menu() {
        let menu = UIMenu.container();
        let menucontents = UIMenu.multi_node("div",
            UIMenu.title_logo(),
            UIMenu.title_button("Play", ev => {
                UIMenu.title_active = false;
                menu.remove();
                Game.startloop();
                Draw.CANVAS.focus();
            }),
            UIMenu.title_button("Leaderboard", ev => {
                UIMenu.show_leaderboard_menu();
            }),
            UIMenu.title_button("Change Theme", ev => {
                UIMenu.title_active = true;
                menu.remove();
                UIMenu.show_theme_menu();
            }),
            UIMenu.title_button("Show/Hide Console", ev => {
                Console.toggle();
            }),
        );

        menucontents.className = "uimenu_title_menu";
        menu.appendChild(menucontents);
        return menu;
    }

    static title_logo() {
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d");

        canvas.width = 500;
        canvas.height = 100;

        ctx.fillStyle = Draw.TEXT_COLOR_FG;
        ctx.font = "bold 80px monospace";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText("FREE RINGS", 500 / 2, 100 / 2);

        return canvas;
    }

    static leaderboard_placeholder() {
        let menu = UIMenu.container();
        let menucontents = UIMenu.multi_node(
            "div",
            UIMenu.input_button("cancel", ev => {
                menu.remove();
                UIMenu.leaderboard_canceled = true;
            }),
            UIMenu.text_node("div", "Loading Leaderboard..."),
        );

        menucontents.className = "uimenu_leaderboard_menu";
        menu.appendChild(menucontents);
        return menu;
    }

    static async leaderboard_menu(offset) {
        let menu = UIMenu.container();
        let ownscore_elem = await UIMenu.leaderboard_own_score();
        let listing_elem = await UIMenu.leaderboard_listing(offset);
        let menucontents = UIMenu.multi_node(
            "div",
            UIMenu.input_button("close", ev => {
                menu.remove();
                Draw.CANVAS.focus();
            }),
            UIMenu.section_label("Leaderboard:"),
            UIMenu.leaderboard_nav(offset, UIMenu.lb_total_runs),
            listing_elem,
            UIMenu.section_label("Your Score:"),
            ownscore_elem,
        );

        menucontents.className = "uimenu_leaderboard_menu";
        menu.appendChild(menucontents);
        return menu;
    }

    static leaderboard_nav(offset, totalruns) {
        const go_callback = _ => {
            let offset = (text.value|0) - 1;

            if (offset < 0)
                return;

            if (offset >= UIMenu.lb_total_runs - 1)
                offset = UIMenu.lb_total_runs - UIMenu.LB_PER_PAGE;

            UIMenu.show_leaderboard_menu(offset);
        };
        let info = UIMenu.text_node(
            "span",
            "showing %start-%end of %total"
            .replace("%start", offset + 1)
            .replace("%end",   Math.min(offset + UIMenu.LB_PER_PAGE, UIMenu.lb_total_runs))
            .replace("%total", totalruns)
        );
        let prev = UIMenu.input_button("prev", ev => {
            let offset = UIMenu.lb_current_offset - UIMenu.LB_PER_PAGE;

            if (offset < 0)
                offset = 0;

            UIMenu.show_leaderboard_menu(offset);
        });
        let next = UIMenu.input_button("next", ev => {
            let offset = UIMenu.lb_current_offset + UIMenu.LB_PER_PAGE;

            if (offset > UIMenu.lb_total_runs - 1)
                return;

            UIMenu.show_leaderboard_menu(offset);
        });
        let go = UIMenu.input_button("go", go_callback);
        let text = UIMenu.input_text(go_callback);
        let navbar = UIMenu.multi_node("div", info, prev, next, text, go);

        info.className = "inline_label";
        text.value = (offset|0) + 1;
        navbar.className = "leaderboard_nav";
        return navbar;
    }

    static leaderboard_entry(lbentry) {
        let showmorebtn_active = false;
        let showmorebtn = UIMenu.table_button_inline("show more", ev => {
            showmorebtn_active = !showmorebtn_active;

            if (showmorebtn_active) {
                ev.target.value = "show less";
                rowels.slice(1).forEach(rowel => {
                    rowel.style.display = "table-row";
                });
            }
            else {
                ev.target.value = "show more";
                rowels.slice(1).forEach(rowel => {
                    rowel.style.display = "none";
                });
            }
        });
        let reportbtn = UIMenu.table_button_inline("report", ev => {
            if (lbentry.id === UIMenu.own_score.id)
                return UIMenu.alert("You cannot report yourself.");

            UIMenu.stringform("Reason for report (e.g. offensive user name, cheated score)", response => {
                Leaderboard.report_run(lbentry.id, response);
            });
        });
        let rowels = [
            UIMenu.table_row(lbentry.position, lbentry.name, showmorebtn),
            UIMenu.table_row_hidden("Actions", reportbtn),
            UIMenu.table_row_hidden("Score", lbentry.score),
            UIMenu.table_row_hidden("Clears", lbentry.clears),
            UIMenu.table_row_hidden("Round", lbentry.round),
            UIMenu.table_row_hidden("Time", Util.ms_to_str(lbentry.endTimestamp - lbentry.startTimestamp)),
            UIMenu.table_row_hidden("&nbsp;"),
        ];

        rowels.forEach(row => {
            row.className = "leaderboard_entry";

            if (lbentry.id === UIMenu.own_score.id)
                row.className += " own_score";
        });

        return rowels;
    }

    static async leaderboard_listing(offset) {
        let lb_contents = await Leaderboard.get_leaderboard(offset, UIMenu.LB_PER_PAGE);
        let table = document.createElement("table");

        UIMenu.lb_current_offset = offset;
        UIMenu.lb_total_runs = lb_contents.total;

        lb_contents.runs
            .map(UIMenu.leaderboard_entry)
            .forEach(lbentry_rows => {
                lbentry_rows.forEach(row => {
                    table.appendChild(row);
                })
            });

        return table;
    }

    static async leaderboard_own_score() {
        let self_contents = await Leaderboard.get_own_score();
        let table = document.createElement("table");
        let refreshbtn = UIMenu.table_button_inline("refresh", ev => {
            UIMenu.show_leaderboard_menu(UIMenu.lb_current_offset);
        });
        let namechangebtn = UIMenu.table_button_inline("change", async ev => {
            UIMenu.stringform("Enter a new name", async response => {
                if (response === "" || response === null)
                    return;

                if (await Leaderboard.set_name(response))
                    UIMenu.show_leaderboard_menu(UIMenu.lb_current_offset);
            });
        });
        let rowels = [
            UIMenu.table_row_hidden(refreshbtn),
            UIMenu.table_row("Name",     self_contents.name, namechangebtn),
            UIMenu.table_row("Position", self_contents.position),
            UIMenu.table_row("Score",    self_contents.score),
            UIMenu.table_row("Clears",   self_contents.clears),
            UIMenu.table_row("Round",    self_contents.round),
            UIMenu.table_row("Time",     Util.ms_to_str(self_contents.endTimestamp - self_contents.startTimestamp)),
        ];

        UIMenu.own_score = self_contents;

        if (self_contents.id === -1)
            rowels[0].style.display = "table-row";

        rowels.forEach(row => {
            table.appendChild(row);
            row.className = "own_score_entry";
        });

        return table;
    }

    static stringform(prompt_text, callback) {
        const mkcallback = get_value => (ev => {
            callback(get_value());
            menu.remove();
            background.remove();
        });
        let menu = UIMenu.modal_container();
        let background = UIMenu.modal_background();
        let inputel = UIMenu.input_text(mkcallback(v => inputel.value));
        let menucontents = UIMenu.multi_node(
            "div",
            UIMenu.text_node("p", prompt_text),
            inputel,
            UIMenu.input_button("Submit", mkcallback(v => inputel.value)),
            UIMenu.input_button("Cancel", mkcallback(v => null)),
        );

        menucontents.className = "uimenu_modal_stringform";
        menu.appendChild(menucontents);
        document.body.appendChild(background);
        document.body.appendChild(menu);

        if (Game.looprunning)
            Game.pause();
    }

    static alert(prompt_text, okbtn_text) {
        let menu = UIMenu.modal_container();
        let background = UIMenu.modal_background();
        let menucontents = UIMenu.multi_node(
            "div",
            UIMenu.text_node("p", prompt_text),
            UIMenu.input_button(okbtn_text || "Ok", ev => {
                menu.remove();
                background.remove();
            }),
        );

        menucontents.className = "uimenu_modal_alert";
        menu.appendChild(menucontents);
        document.body.appendChild(background);
        document.body.appendChild(menu);

        if (Game.looprunning)
            Game.pause();
    }

    static container(...children) {
        let cont = UIMenu.multi_node("div", ...children);

        cont.className = `uimenu_container theme_${Draw.current_theme}`;

        return cont;
    }

    static multi_node(tagname, ...children) {
        let node = document.createElement(tagname);

        children.forEach(child => {
            node.appendChild(child);
        });

        return node;
    }

    static table_row(...columns) {
        let columnels = columns.map(col => {
            if (col instanceof HTMLElement)
                return col;

            return UIMenu.text_node("td", col);
        });

        return UIMenu.multi_node("tr", ...columnels);
    }

    static table_row_hidden(...columns) {
        let row = UIMenu.table_row(...columns);

        row.style.display = "none";
        return row;
    }

    static text_node(tagname, text) {
        let node = document.createElement(tagname);

        node.innerHTML = text;

        return node;
    }

    static section_label(text) {
        let node = UIMenu.text_node("p", text);

        node.className = "uimenu_section_label";

        return node;
    }

    static input_button(label, callback) {
        let btn = document.createElement("input");

        btn.type = "button";
        btn.value = label;
        btn.addEventListener("click", callback);
        btn.className = "uimenu_button";

        return btn;
    }

    static title_button(label, callback) {
        let button = UIMenu.input_button(label, callback);

        button.className += " block";
        return button;
    }

    static table_button_inline(label, callback) {
        let button = UIMenu.input_button(label, callback);

        button.className += " inline";
        return UIMenu.multi_node("td", button);
    }

    static input_text(callback) {
        let text = document.createElement("input");

        text.addEventListener("keyup", ev => {
            if (ev.key === "Enter")
                callback(ev);
        });

        text.className = "uimenu_input_text";
        return text;
    }

    static modal_container(...children) {
        let cont = UIMenu.multi_node("div", ...children);

        cont.className = "uimenu_modal";
        cont.style.left = ((window.innerWidth - cont.offsetWidth) / 2) + "px";
        cont.style.top = ((window.innerHeight - cont.offsetHeight) / 2) + "px";

        return cont;
    }

    static modal_background() {
        let node = document.createElement("div");

        node.className = "uimenu_modal_background";
        return node;
    }
}
