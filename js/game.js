/*
 * Game - houses main game loop and manages other classes
 *
 * Constants
 *     VERSION: string
 *         game version string
 *
 *     TEXTAREA: DOM Element
 *         reference to stats textarea
 *
 *     RANDOM_BAGS: array<array<int>>
 *         array of random bags used to tune the randomness per-round
 *
 * State
 *     tdelta: float
 *         real time elapsed between the last two frames
 *
 *     looprunning: bool
 *         game loop flag
 *
 *     paused: bool
 *         pause flag
 *
 *     time_elapsed: float
 *         time elapsed since beginning of game (excluding paused time)
 *
 *     score: int
 *         game score
 *
 *     stacks_cleared: int
 *         number of ring stacks or ring sets cleared
 *
 *     current_round: int
 *         current round (10 clears = 1 round)
 *
 *     laststamp: float
 *         the value of the last timestamp (laststamp + tdelta = now)
 *
 *     tickno: int
 *         number of frames or ticks since beginning of game
 *
 *     random_bag: array<int>
 *         copy of random bag (see Game.RANDOM_BAGS)
 *
 *     cheats_suspected: bool
 *         flag used by anticheat to track whether the player probably
 *         cheated
 *
 *     last_slice_timestamp: int
 *         timestamp of last slice
 *
 * Static methods
 *     init()
 *         initializes the game, called by Main.init_all
 *
 *     restart()
 *         if the game loop is not running, calls Main.init_all
 *
 *     random_stack() -> stackid (int)
 *         returns a random stackid (excludes center stack)
 *
 *     random_bag_shuffle()
 *         repopulates and shuffles the random bag
 *
 *     random_ring() -> ringtype (int)
 *         returns top ring from the random bag
 *
 *     get_stats_info() -> string
 *         returns a string representation of game stats
 *
 *     tick(curstamp)
 *         runs a tick, storing the difference between the current and
 *         previous timestamp in tdelta, updating the elapsed time, etc.
 *
 *     loop(timestamp)
 *         runs a single frame of the game loop
 *
 *     check_shadow()
 *         checks if certain variables changed magically
 *
 *     update_slices()
 *         updates slice timestamp and adds a slice every 10 seconds
 *
 *     update_score(score)
 *         updates game score, current round stack clear data
 *
 *     pause()
 *         pauses the game and resets mouse drag
 *
 *     unpause()
 *         unpauses the game, resets mouse drag, and restarts all active
 *         animations. I chose to restart them (starttime = now) rather
 *         than resume them (starttime = now - progress) because it
 *         seemed more fair. Imagine unpausing with half a second left on
 *         the ring timer.
 *
 *     toggle_pause()
 *         toggles between paused and unpaused state
 *
 *     kill()
 *         signals the game loop to terminate
 *
 *     startloop()
 *         starts the ring_init->ring_timer->ring_move animation loop and
 *         the game loop
 */

class Game {
    static VERSION = "1.8.7";
    static TEXTAREA = document.querySelector(".textarea.info");
    static RANDOM_BAGS = [
        [1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3, 0, 1, 2, 3],
        [0, 1, 2, 3, 2, 3],
        [0, 1, 2, 3, 0, 1, 2, 3, 2, 3],
    ];
    static tdelta;
    static looprunning;
    static paused;
    static time_elapsed;
    static score;
    static stacks_cleared;
    static current_round;
    static laststamp;
    static tickno;
    static random_bag;
    static cheats_suspected;
    static last_slice_timestamp;

    static init() {
        Game.tdelta = 0;
        Game.looprunning = false;
        Game.paused = 0;
        Game.time_elapsed = 0;
        Game.score = 0;
        Game.stacks_cleared = 0;
        Game.current_round = 1;
        Game.laststamp = null;
        Game.tickno = 0;
        Game.random_bag = [];
        Game.cheats_suspected = false;
        Game.last_slice_timestamp = null;
    }

    static restart() {
        if (!Game.looprunning) {
            Main.init_all();
            Game.startloop();
        }
    }

    static random_stack() {
        return Math.floor(Math.random() * (Stack.NSTACKS - 1));
    }

    static random_bag_shuffle() {
        const rand_cmp = (a_unused, b_unused) => Math.floor(Math.random() * 3) - 1;
        let selbag_idx = Math.floor(Game.current_round / 2);
        let selbag;

        if (!(selbag_idx in Game.RANDOM_BAGS))
            selbag_idx = Game.current_round % Game.RANDOM_BAGS.length

        selbag = Game.RANDOM_BAGS[selbag_idx];
        Game.random_bag = [...selbag].sort(rand_cmp);
    }

    /* The new RNG is experimental and uses a tetris-like random bag
     * without the legality test. 8 rings shuffled means a worst case of
     * 4 of the same ring in a row ([0, 1, ... 3, 3] ->
     * [3, 3, 1, 2, 1, ...])
     *
     * After playtesting it, it feels so much fairer.
     */
    static random_ring() {
        if (Game.random_bag.length === 0)
            Game.random_bag_shuffle();

        return Game.random_bag.pop();
    }

    static get_stats_info() {
        return `` +
            `Free Rings v${Game.VERSION}\n` +
            ((Game.cheats_suspected) ? (`! (Cheats suspected)\n`) : ("")) +
            `Time:   ${Util.ms_to_str(Game.time_elapsed)}\n` +
            `Round:  ${Game.current_round}\n` +
            `Clears: ${Game.stacks_cleared}\n` +
            `Score:  ${Game.score}\n` +
            `FPS:    ${Math.floor(1000 / Game.tdelta)}\n` +
            `Frame:  ${Game.tickno}\n` +
            `Theme:  ${Draw.current_theme}\n` +
            `Rings:  ${Draw.current_ring_theme}\n` +
            `` ;
    }

    static tick(curstamp) {
        let diff;

        if (Game.laststamp === null) {
            Game.laststamp = curstamp;
            return;
        }

        diff = curstamp - Game.laststamp;
        Game.laststamp = curstamp;
        Game.tdelta = diff;
        Game.time_elapsed += Game.tdelta;
        ++Game.tickno;
    }

    static update_slices() {
        if (Game.last_slice_timestamp === null) {
            Game.last_slice_timestamp = Game.laststamp;
            return;
        }

        if (Game.laststamp - Game.last_slice_timestamp > 10000) {
            Anticheat.make_slice();
            Game.last_slice_timestamp = Game.laststamp;
        }
    }

    static update_score(score) {
        if (score === 0)
            return;

        if (++Game.stacks_cleared % 10 == 0) {
            ++Game.current_round;
            Stack.reset();
            Animation.push(new Animation({
                type:      "new_round",
                starttime: Game.laststamp,
                roundno:   Game.current_round,
            }));
        }

        Game.score += score;
    }

    /* 1. Fixes a rendering bug where the last frame of the ring_move
     *    animation is visible on game over if the timer ran out. Also
     *    added this to the pause screen cause I thought it looked nice.
     */
    static loop(timestamp) {
        Draw.clear();
        Game.check_shadow();
        Game.update_slices();
        Joypad.poll_axes();
        Joypad.poll_buttons();

        if (Game.paused) {
            Draw.draw_stacks(); // 1
            Draw.draw_pause_screen();
            Game.laststamp = timestamp;
            requestAnimationFrame(Game.loop);
            return;
        }

        Game.tick(timestamp);
        Game.update_score(Stack.check_ringsets());
        Draw.draw_ring_timer_line();
        Draw.draw_stacks();
        Draw.draw_random_bag();
        Animation.step_all(timestamp);
        Draw.draw_joypad_indicator();
        Draw.draw_mouse_ring();
        Game.TEXTAREA.value = Game.get_stats_info();

        if (Stack.check_gameover()) {
            Game.kill();
            Draw.clear();
            Draw.draw_stacks(); // 1
            Draw.draw_game_over();
        }

        if (Game.looprunning)
            requestAnimationFrame(Game.loop);
    }

    static check_shadow() {
        let shadow = [
            Game.tickno,
            Game.current_round,
            Game.stacks_cleared,
            Game.score,
            Game.time_elapsed,
        ];

        if (Anticheat.check_shadow(shadow))
            Anticheat.shadow_flag = true;
    }

    static pause() {
        Game.paused = true;
        Mouse.put_held_ring_back();
        Mouse.lb = 0;
    }

    /* Putting the held ring back is an anti-cheese measure. You can
     * pause buffer to buy time on account of the animations restarting
     * (which is done to prevent them from breaking), but with this patch
     * all this will do for you is allow you more time to think
     */
    static unpause() {
        Game.paused = false;
        Animation.restart_animations();
        Mouse.put_held_ring_back();
        Mouse.lb = 0;
        Draw.CANVAS.focus();
    }

    static toggle_pause() {
        if (Game.paused)
            Game.unpause();
        else
            Game.pause();
    }

    static async kill() {
        let response;

        Game.looprunning = false;
        Anticheat.end_timestamp = Date.now();

        if (!Anticheat.validate_run() || Game.cheats_suspected) {
            Console.warn("Run not submitted (cheats suspected)");
            return;
        }

        Anticheat.kill();
        Console.info("submiting run...");
        Game.TEXTAREA.value = Game.get_stats_info() + "\n\nsubmitting run...";

        response = await Leaderboard.submit_run();

        if (response !== null) {
            Console.info("submitted.");
            Game.TEXTAREA.value = Game.get_stats_info() + "\n\nsubmitted.";

            if (response.isPersonalBest)
                Game.TEXTAREA.value += "\nNew High Score!";
        }
        else {
            Console.error("submit failed.");
            Game.TEXTAREA.value = Game.get_stats_info() + "\n\nsubmit failed.";
        }
    }

    static startloop() {
        Game.looprunning = true;
        Animation.push(new Animation({
            type:      "ring_init",
            starttime: 0,
        }));
        requestAnimationFrame(Game.loop);
    }
}
