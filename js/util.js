/*
 * Util - misc utility functions
 *
 * Static Methods
 *     zeropad(value, width) -> string
 *         returns string repr of value left-zero-padded to width
 *
 *     ms_to_str(ms) -> string
 *         returns string repr of ms in (h:)mm:ss.sss format
 *
 *     obj_to_search(obj) -> string
 *         returns string {a: 1, b: 2} => "?a=1&b=2"
 *
 *     sorted_insert(array, value, cmp)
 *         inserts value into array in the order specified by cmp
 *
 *     sleep(ms) -> Promise
 *         sleeps for the specified number of milliseconds
 *
 *     fetch(url, request) -> Promise
 *         wraps built-in fetch with logging
 *
 *     generate_uuid()
 *         generates a compliant UUID without the crypto interface
 */

class Util {
    static zeropad(value, width) {
        let str = value.toString();

        while (str.length < width)
            str = "0" + str;

        return str;
    }

    static ms_to_str(ms) {
        const SECOND = 1000;
        const MINUTE = SECOND * 60;
        const HOUR = MINUTE * 60;
        let elapsed = Math.floor(ms);
        let msec;
        let sec;
        let minutes;
        let hours;
        let str = "";

        if (isNaN(ms))
            return "N/A";

        hours = Math.floor(elapsed / HOUR);
        minutes = Math.floor((elapsed % HOUR) / MINUTE);
        sec = Math.floor((elapsed % MINUTE) / SECOND);
        msec = elapsed % SECOND;

        if (hours > 0)
            str += `${hours.toString()}:`;

        str += `${Util.zeropad(minutes, 2)}:${Util.zeropad(sec, 2)}.${Util.zeropad(msec, 3)}`;
        return str;
    }

    static obj_to_search(obj) {
        return "?" + Object.keys(obj)
            .map(key => `${key}=${obj[key]}`)
            .join("&");
    }

    static sorted_insert(array, value, cmp) {
        for (let i = 0; i < array.length; ++i)
            if (cmp(array[i], value) > 0)
                return array.splice(i, 0, value);

        return array.push(value);
    }

    static async sleep(ms) {
        return new Promise(_ => setTimeout(_, ms));
    }

    static async fetch(url, request) {
        Console.info(`fetch: ${request?.method || "GET"} ${url}`);
        Console.info(`request body: ${JSON.stringify(request?.body || {})}`);

        while(true) {
            let response = await fetch(url, request).catch(err => {
                Console.error(err);
                return null;
            });

            if (response !== null)
                return response;

            Console.info(`Retrying in 10 seconds...`);
            await Util.sleep(10000);
        }

        return null; // unreachable
    }

    static generate_uuid() {
        const fmtrange = (start, len) => {
            let out = [];

            for (let i = 0; i < len; ++i) {
                let hexpair = bytes[start + i].toString(16);

                if (hexpair.length === 1)
                    hexpair = "0" + hexpair;

                out.push(hexpair);
            }

            return out.join("");
        }
        let bytes = [];
        let chunks = [];

        for (let i = 0; i < 16; ++i)
            bytes.push(Math.floor(Math.random() * (1<<16)) % 256);

        bytes[6] = (bytes[6] & 0x4F) | 0x40; // 0100XXXX
        bytes[8] = (bytes[8] & 0xBF) | 0x80; // 10XXXXXX
        chunks.push(fmtrange(0, 4));  // XXXXXXXX-
        chunks.push(fmtrange(4, 2));  // XXXX-
        chunks.push(fmtrange(6, 2));  // 4XXX-
        chunks.push(fmtrange(8, 2));  // XXXX-
        chunks.push(fmtrange(10, 6)); // XXXXXXXXXXXX

        return chunks.join("-");
    }
}
