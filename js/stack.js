/*
 * Stack - manages the 6 ring stacks + center stack
 *
 * Constants
 *     RING_SIZES: array<int>
 *         sizes of rings 0-3, bg, hitbox and random bag rings
 *
 *     RING_INNER_SIZE: int
 *         size of hole in the center of the rings
 *
 *     NSTACKS: int
 *         number of stacks including center stack
 *
 *     COORDS: array<array<int>>
 *         coordinates of all 7 ring stacks
 *
 *     STACKS_TEMPLATE: array<array<int>>
 *         the default state of stacks[]
 *
 * State
 *     stacks: array<array<int>>
 *         array where rings go. The 7th stack is the center stack and
 *         is where rings are spawned from
 *
 * Static methods
 *     init()
 *         called by Main.init_all()
 *
 *     reset()
 *         Resets stacks[] for a new round
 *
 *     which_stack(x, y) -> stackid (int)
 *         returns the ID of the stack (0-6) that the mouse is over, or
 *         -1 for none
 *
 *     send_ring() -> bool
 *         interrupts ring_timer animation if the move is legal. Returns
 *         false if unable to find ring_timer animation
 *
 *     check_gameover() -> bool
 *         checks if stacks 0-5 contain two rings in the wrong order
 *
 *     check_ringsets() -> int
 *         removes rings from stacks and returns the accumulated score
 *         a "ringset" is B-Y, R-B-Y, or G-R-B-Y
 *
 *     center_empty() -> bool
 *         tells whether the center stack is empty
 *
 *     push(stackid, ring)
 *         pushes ring to stacks[stackid], keeps center stack to a
 *         length of 1.
 *
 *     pop(stackid) -> ringtype (int)
 *         removes ring at top of stacks[stackid] and returns it
 *
 *     get_coords(stackid) -> array<int>
 *         returns copy of coordinates of stacks[stackid]
 *
 *     get_stack(stackid) -> array<int>
 *         returns copy of stacks[stackid]
 */

class Stack {
    // 0, 1, 2, 3, grey, hitbox, tiny (random bag preview)
    static RING_SIZES = [50, 40, 30, 15, 50, 90, 15];
    static RING_INNER_SIZE = 20;
    static NSTACKS = 7;
    static COORDS = [
        [170, 70],
        [330, 70],
        [70,  250],
        [430, 250],
        [170, 430],
        [330, 430],
        [250, 250],
    ];
    static STACKS_TEMPLATE = [
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    ];
    static stacks;

    static init() {
        Stack.reset();
    }

    static reset() {
        Stack.stacks = Stack.STACKS_TEMPLATE.map(arr => []);
    }

    /* The detection range is now much larger than the grey circles, so
     * it feels a lot better to play now
     */
    static which_stack(x, y) {
        for (let i = 0; i < Stack.COORDS.length; ++i)
            if (Draw.path_circle(Stack.COORDS[i][0], Stack.COORDS[i][1], Stack.RING_SIZES[5], ctx => ctx.isPointInPath(x, y)))
                return i;

        return -1;
    }

    static send_ring() {
        let selanim;
        let ring;
        let stacktarget;
        let stacktargettop;

        selanim = Animation.list.find(anim => anim.type === "ring_timer");

        if (typeof selanim === "undefined")
            return false;

        ring = Stack.get_stack(6).pop();
        stacktarget = Stack.get_stack(selanim.selstack);
        stacktargettop = [...stacktarget].pop();

        if (stacktarget.length === 0 || stacktargettop < ring)
            selanim.interrupt = 1;

        return true;
    }

    static check_gameover() {
        let results = Stack.stacks
            .slice(0, Stack.NSTACKS - 1)
            .filter(stack => {
                for (let i = 0; i < stack.length - 1; ++i)
                    if (stack[i] >= stack[i + 1])
                        return true;

                return false;
            });

        return results.length > 0;
    }

    static check_ringsets() {
        let total = 0;

        for (let i = 0; i < Stack.NSTACKS - 1; ++i) {
            let selstackcp = Stack.get_stack(i);

            if (selstackcp.pop() !== 3)
                continue;

            if (selstackcp.pop() !== 2)
                continue;

            total += 50;
            Stack.pop(i);
            Stack.pop(i);

            if (selstackcp.pop() !== 1)
                continue;

            total += 50;
            Stack.pop(i);

            if (selstackcp.pop() !== 0)
                continue;

            total += 150;
            Stack.pop(i);
        }

        return total;
    }

    static center_empty() {
        let selstack = Stack.get_stack(6);

        return selstack.length === 0;
    }

    static push(stackid, ring) {
        let selstack = Stack.stacks[stackid];

        if (stackid === 6 && selstack.length > 0)
            selstack.pop();

        selstack.push(ring);

        if (typeof ring !== "number")
            throw "Stack.push received non-number. Please investigate.";
    }

    static pop(stackid) {
        let selstack = Stack.stacks[stackid];

        return selstack.pop();
    }

    static get_coords(stackid) {
        return [...Stack.COORDS[stackid]];
    }

    static get_stack(stackid) {
        let selstack = Stack.stacks[stackid];

        return [...selstack];
    }
}
