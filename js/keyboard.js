/*
 * Keyboard - Manages keyboard events
 *
 * Constants
 *     KEYMAPS: array<array<string, fn>>
 *         maps keyboard keys to functions
 *
 * State
 *     keysdown: map<string, bool>
 *         tracks which keys are and aren't down
 *
 * Static Methods
 *     init()
 *         called by Input.init. Registers events, sets control mappings
 *         and resets any mappings that might be active
 *
 *     snap_ring_to(stackid) -> fn
 *         returns function that teleports mouse held ring to specific
 *         stack (stackid) if the move is legal.
 *
 *     ev_keyup(ev)
 *         sets affected key to not pressed
 *
 *     ev_keydown(ev)
 *         sets affected key to pressed, prevents default, and then
 *         searches the mappings to see if any of them need to be
 *         executed
 */

class Keyboard {
    static KEYMAPS;
    static keysdown;

    static init() {
        Keyboard.KEYMAPS = [
            [" ", Stack.send_ring],
            ["r", Game.restart],
            ["l", Leaderboard.show_leaderboard],
            ["p", Game.toggle_pause],
            ["t", UIMenu.show_theme_menu],
            ["c", Console.toggle],
            ["1", Keyboard.snap_ring_to(0)],
            ["2", Keyboard.snap_ring_to(1)],
            ["3", Keyboard.snap_ring_to(2)],
            ["4", Keyboard.snap_ring_to(3)],
            ["5", Keyboard.snap_ring_to(4)],
            ["6", Keyboard.snap_ring_to(5)],
        ];

        Keyboard.keysdown = {};
        Input.addev("keydown", Keyboard.ev_keydown);
        Input.addev("keyup",   Keyboard.ev_keyup);

        Keyboard.KEYMAPS.forEach(mapping => {
            Keyboard.keysdown[mapping[0]] = false;
        });
    }

    static snap_ring_to(stackid) {
        return _ => {
            let stackcp = Stack.get_stack(stackid);

            if (!Mouse.lb || typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
                return;

            if (stackcp.length === 0 || stackcp.pop() < Mouse.heldring) {
                Stack.push(stackid, Mouse.heldring);
                Mouse.heldring = -1;
                Mouse.heldringstack = -1;
            }
        }
    }

    static ev_keyup(ev) {
        Keyboard.keysdown[ev.key] = false;
    }

    static ev_keydown(ev) {
        ev.preventDefault();
        Keyboard.keysdown[ev.key] = true;

        Keyboard.KEYMAPS.forEach(mapping => {
            if (Keyboard.keysdown[mapping[0]])
                mapping[1]();
        });
    }
}
